package dwfe.modules.mailing;

import dwfe.test.DwfeTestChecker;

import java.util.List;
import java.util.Map;

public class MailingTestVariablesForApiTest
{

  //-------------------------------------------------------
  // Personal
  //

  public static final List<DwfeTestChecker> checkers_for_personal = List.of(
          DwfeTestChecker.of(false, 200, Map.of(), "missing-module"),
          DwfeTestChecker.of(false, 200, Map.of("module", ""), "empty-module"),
          DwfeTestChecker.of(false, 200, Map.of("module", "NEVIS"), "missing-type"),
          DwfeTestChecker.of(false, 200, Map.of("module", "NEVIS", "type", ""), "empty-type"),
          DwfeTestChecker.of(false, 200, Map.of("module", "NEVIS", "type", "WELCOME_ONLY"), "missing-email"),
          DwfeTestChecker.of(false, 200, Map.of("module", "NEVIS", "type", "WELCOME_ONLY", "email", ""), "empty-email"),

          DwfeTestChecker.of(true, 200, Map.of("module", "NEVIS", "type", "WELCOME_ONLY", "email", "test1@dwfe.ru")),
          DwfeTestChecker.of(true, 200, Map.of("module", "NEVIS", "type", "WELCOME_PASSWORD", "email", "test1@dwfe.ru", "data", "1234567890")),
          DwfeTestChecker.of(true, 200, Map.of("module", "NEVIS", "type", "PASSWORD_WAS_CHANGED", "email", "test1@dwfe.ru")),
          DwfeTestChecker.of(true, 200, Map.of("module", "NEVIS", "type", "PASSWORD_RESET_CONFIRM", "email", "test1@dwfe.ru", "data", "12121212")),
          DwfeTestChecker.of(true, 200, Map.of("module", "NEVIS", "type", "EMAIL_CONFIRM", "email", "test1@dwfe.ru", "data", "kjsjkjsksjsksj"))
  );
}
