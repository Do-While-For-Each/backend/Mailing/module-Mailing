package dwfe.modules.mailing.controller;

import dwfe.modules.mailing.db.mailing.MailingPersonal;
import dwfe.modules.mailing.db.mailing.MailingPersonalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

import static dwfe.util.DwfeUtil.getResponse;
import static dwfe.util.DwfeUtil.isDefaultPreCheckOk;

@RestController
@RequestMapping(path = "#{mailingConfigProperties.api}", produces = "application/json; charset=utf-8")
public class MailingController
{
  private final MailingPersonalService mailingPersonalService;

  @Autowired
  public MailingController(MailingPersonalService mailingPersonalService)
  {
    this.mailingPersonalService = mailingPersonalService;
  }

  @PostMapping("#{mailingConfigProperties.resource.personal}")
  public String personal(@RequestBody ReqPersonal req)
  {
    var errorCodes = new ArrayList<String>();
    var module = req.module;
    var type = req.type;
    var email = req.email;

    if (isDefaultPreCheckOk(module, req.moduleFieldName, errorCodes)
            && isDefaultPreCheckOk(type, req.typeFieldName, errorCodes)
            && isDefaultPreCheckOk(email, req.emailFieldName, errorCodes))
    {
      mailingPersonalService.save(MailingPersonal.of(module, type, email, req.data));
    }
    return getResponse(errorCodes);
  }
}

class ReqPersonal
{
  String module;
  String type;
  String email;
  String data;

  String moduleFieldName = "module";
  String typeFieldName = "type";
  String emailFieldName = "email";

  public String getModule()
  {
    return module;
  }

  public void setModule(String module)
  {
    this.module = module;
  }

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getData()
  {
    return data;
  }

  public void setData(String data)
  {
    this.data = data;
  }
}
