package dwfe.modules.mailing.db.mailing;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicInteger;

import static dwfe.util.DwfeUtil.formatDateTimeToUTCstring;
import static dwfe.util.DwfeUtil.getJsonFieldFromObj;

@Entity
@IdClass(MailingPersonal.MailingId.class)
@Table(name = "dwfe_mailing_personal")
public class MailingPersonal implements Comparable<MailingPersonal>
{
  @Id
  @Column(updatable = false, insertable = false)
  private LocalDateTime createdOn;

  @Id
  private String module;

  @Id
  private String type;

  @Id
  private String email;

  private String data;
  private volatile boolean sent;
  private volatile boolean maxAttemptsReached;

  private String causeOfLastFailure;

  @Column(updatable = false, insertable = false)
  private LocalDateTime updatedOn;

  @Transient
  private AtomicInteger attempt = new AtomicInteger(0);

  public static MailingPersonal of(String module, String type, String email, String data)
  {
    var mailing = new MailingPersonal();
    mailing.setCreatedOn(LocalDateTime.now());
    mailing.setType(type);
    mailing.setEmail(email);
    mailing.setModule(module);
    mailing.setData(data);
    mailing.setSent(false);
    mailing.setMaxAttemptsReached(false);
    return mailing;
  }

  public static MailingPersonal of(String module, String type, String email)
  {
    return of(module, type, email, "");
  }

  public void clear()
  {
    data = null;
  }


  //
  //  GETTERs and SETTERs
  //

  public LocalDateTime getCreatedOn()
  {
    return createdOn;
  }

  public void setCreatedOn(LocalDateTime createdOn)
  {
    this.createdOn = createdOn;
  }

  public String getModule()
  {
    return module;
  }

  public void setModule(String module)
  {
    this.module = module;
  }

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getData()
  {
    return data;
  }

  public void setData(String data)
  {
    this.data = data;
  }

  public boolean isSent()
  {
    return sent;
  }

  public void setSent(boolean sent)
  {
    this.sent = sent;
  }

  public boolean isMaxAttemptsReached()
  {
    return maxAttemptsReached;
  }

  public void setMaxAttemptsReached(boolean maxAttemptsReached)
  {
    this.maxAttemptsReached = maxAttemptsReached;
  }

  public String getCauseOfLastFailure()
  {
    return causeOfLastFailure;
  }

  public void setCauseOfLastFailure(String causeOfLastFailure)
  {
    this.causeOfLastFailure = causeOfLastFailure;
  }

  public LocalDateTime getUpdatedOn()
  {
    return updatedOn;
  }

  public AtomicInteger getAttempt()
  {
    return attempt;
  }


  //
  //  equals, hashCode, compareTo, toString
  //

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    MailingPersonal mailing = (MailingPersonal) o;

    if (!createdOn.equals(mailing.createdOn)) return false;
    if (!module.equals(mailing.module)) return false;
    if (!type.equals(mailing.type)) return false;
    return email.equals(mailing.email);
  }

  @Override
  public int hashCode()
  {
    int result = createdOn.hashCode();
    result = 31 * result + module.hashCode();
    result = 31 * result + type.hashCode();
    result = 31 * result + email.hashCode();
    return result;
  }

  @Override
  public int compareTo(MailingPersonal o)
  {
    var result = 0;
    if ((result = module.compareTo(o.module)) == 0)
      if ((result = createdOn.compareTo(o.createdOn)) == 0)
        if ((result = type.compareTo(o.type)) == 0)
          result = email.compareTo(o.email);
    return result;
  }

  @Override
  public String toString()
  {
    return "{\n" +
            " " + getJsonFieldFromObj("module", module) + ",\n" +
            " \"createdOn\": " + "\"" + formatDateTimeToUTCstring(createdOn) + "\",\n" +
            " " + getJsonFieldFromObj("type", type) + ",\n" +
            " " + getJsonFieldFromObj("email", email) + ",\n" +
            " \"data\": \"****\",\n" +
            " \"sent\": " + sent + ",\n" +
            " \"maxAttemptsReached\": " + maxAttemptsReached + ",\n" +
            " " + getJsonFieldFromObj("causeOfLastFailure", causeOfLastFailure) + ",\n" +
            " \"updatedOn\": " + "\"" + formatDateTimeToUTCstring(updatedOn) + "\"\n" +
            "}";
  }


  //
  // OTHER
  //

  public static class MailingId implements Serializable
  {
    private String module;
    private LocalDateTime createdOn;
    private String type;
    private String email;

    @Override
    public boolean equals(Object o)
    {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      MailingId mailingId = (MailingId) o;

      if (!module.equals(mailingId.module)) return false;
      if (!createdOn.equals(mailingId.createdOn)) return false;
      if (!type.equals(mailingId.type)) return false;
      return email.equals(mailingId.email);
    }

    @Override
    public int hashCode()
    {
      int result = module.hashCode();
      result = 31 * result + createdOn.hashCode();
      result = 31 * result + type.hashCode();
      result = 31 * result + email.hashCode();
      return result;
    }
  }
}
