package dwfe.modules.mailing.db.mailing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MailingPersonalRepository extends JpaRepository<MailingPersonal, MailingPersonal.MailingId>
{
  @Query(nativeQuery = true,
          value = "SELECT * FROM dwfe_mailing_personal WHERE sent=false AND max_attempts_reached=false")
  List<MailingPersonal> getNewJob();

  List<MailingPersonal> findByModuleAndEmail(String module, String email);

  List<MailingPersonal> findByModuleAndTypeAndEmail(String module, String type, String email);

  @Query(nativeQuery = true,
          value = "SELECT * FROM dwfe_mailing_personal WHERE module=:module AND type=:type AND email=:email ORDER BY created_on DESC LIMIT 1")
  Optional<MailingPersonal> findLastByModuleAndTypeAndEmail(@Param("module") String module, @Param("type") String type, @Param("email") String email);

  Optional<MailingPersonal> findByModuleAndTypeAndData(String module, String type, String data);

  @Query(nativeQuery = true,
          value = "SELECT * FROM dwfe_mailing_personal WHERE module=:module AND type=:type AND email=:email AND sent=true AND data<>''")
  List<MailingPersonal> findSentNotEmptyData(@Param("module") String module, @Param("type") String type, @Param("email") String email);

  @Query(nativeQuery = true,
          value = "SELECT * FROM dwfe_mailing_personal WHERE module=:module AND type=:type AND email=:email AND sent=true AND data<>'' ORDER BY created_on DESC LIMIT 1")
  Optional<MailingPersonal> findLastSentNotEmptyData(@Param("module") String module, @Param("type") String type, @Param("email") String email);
}
