package dwfe.modules.mailing.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

import static dwfe.util.DwfeUtil.formatMillisecondsToReadableString;

@Validated
@Configuration
@ConfigurationProperties(prefix = "dwfe.mailing")
public class MailingConfigProperties implements InitializingBean
{
  private final static Logger log = LoggerFactory.getLogger(MailingConfigProperties.class);

  private String api;

  @NotNull
  private ScheduledTaskMailing scheduledTaskMailing;

  private Frontend frontend = new Frontend();

  private Resource resource = new Resource();

  @Override
  public void afterPropertiesSet() throws Exception
  {
    log.info(toString());
  }

  public static class ScheduledTaskMailing
  {
    private int initialDelay = 0;

    private int collectFromDbInterval = 60_000; // 1 minute

    private int sendInterval = 30_000; // 30 seconds

    private int maxAttemptsToSendIfError = 3;

    public int getInitialDelay()
    {
      return initialDelay;
    }

    public void setInitialDelay(int initialDelay)
    {
      this.initialDelay = initialDelay;
    }

    public int getCollectFromDbInterval()
    {
      return collectFromDbInterval;
    }

    public void setCollectFromDbInterval(int collectFromDbInterval)
    {
      this.collectFromDbInterval = collectFromDbInterval;
    }

    public int getSendInterval()
    {
      return sendInterval;
    }

    public void setSendInterval(int sendInterval)
    {
      this.sendInterval = sendInterval;
    }

    public int getMaxAttemptsToSendIfError()
    {
      return maxAttemptsToSendIfError;
    }

    public void setMaxAttemptsToSendIfError(int maxAttemptsToSendIfError)
    {
      this.maxAttemptsToSendIfError = maxAttemptsToSendIfError;
    }
  }

  public static class Frontend
  {
    private String host = "http://localhost";

    private String resourceEmailConfirm = "/email-confirm";
    private String resourcePasswordReset = "/password-reset";
    private String resourceAccount = "/account";

    public String getHost()
    {
      return host;
    }

    public void setHost(String host)
    {
      this.host = host;
    }

    public String getResourceEmailConfirm()
    {
      return resourceEmailConfirm;
    }

    public void setResourceEmailConfirm(String resourceEmailConfirm)
    {
      this.resourceEmailConfirm = resourceEmailConfirm;
    }

    public String getResourcePasswordReset()
    {
      return resourcePasswordReset;
    }

    public void setResourcePasswordReset(String resourcePasswordReset)
    {
      this.resourcePasswordReset = resourcePasswordReset;
    }

    public String getResourceAccount()
    {
      return resourceAccount;
    }

    public void setResourceAccount(String resourceAccount)
    {
      this.resourceAccount = resourceAccount;
    }
  }

  public static class Resource
  {
    private String personal = "/personal";

    public String getPersonal()
    {
      return personal;
    }

    public void setPersonal(String personal)
    {
      this.personal = personal;
    }
  }

  public String getApi()
  {
    return api;
  }

  public void setApi(String api)
  {
    this.api = api;
  }

  public ScheduledTaskMailing getScheduledTaskMailing()
  {
    return scheduledTaskMailing;
  }

  public void setScheduledTaskMailing(ScheduledTaskMailing scheduledTaskMailing)
  {
    this.scheduledTaskMailing = scheduledTaskMailing;
  }

  public Frontend getFrontend()
  {
    return frontend;
  }

  public void setFrontend(Frontend frontend)
  {
    this.frontend = frontend;
  }

  public Resource getResource()
  {
    return resource;
  }

  public void setResource(Resource resource)
  {
    this.resource = resource;
  }

  @Override
  public String toString()
  {
    return String.format("%n%n" +
                    "-====================================================-%n" +
                    "|                       Mailing                      |%n" +
                    "|----------------------------------------------------|%n" +
                    "|                                                     %n" +
                    "| API version                       %s%n" +
                    "|                                                     %n" +
                    "|                                                     %n" +
                    "| API Resources                                       %n" +
                    "|      %s%n" +
                    "|                                                     %n" +
                    "|                                                     %n" +
                    "| Scheduled Task - Mailing:                           %n" +
                    "|   initial delay                   %s%n" +
                    "|   collect from DB interval        %s%n" +
                    "|   send interval                   %s%n" +
                    "|   max attempts to send if error   %s%n" +
                    "|                                                     %n" +
                    "|                                                     %n" +
                    "| Frontend                                            %n" +
                    "|   host                            %s%n" +
                    "|   Resources for:                                    %n" +
                    "|     password rest                 %s%n" +
                    "|     email confirm                 %s%n" +
                    "|     account                       %s%n" +
                    "|_____________________________________________________%n%n",
            api,

            // API Resources
            resource.personal,

            // Scheduled Task - Mailing
            formatMillisecondsToReadableString(scheduledTaskMailing.initialDelay),
            formatMillisecondsToReadableString(scheduledTaskMailing.collectFromDbInterval),
            formatMillisecondsToReadableString(scheduledTaskMailing.sendInterval),
            scheduledTaskMailing.maxAttemptsToSendIfError,

            // Frontend
            frontend.host,
            frontend.resourcePasswordReset,
            frontend.resourceEmailConfirm,
            frontend.resourceAccount
    );
  }
}
