package dwfe.modules.mailing.schedule;

import dwfe.modules.mailing.config.MailingConfigProperties;
import dwfe.modules.mailing.db.mailing.MailingPersonal;
import dwfe.modules.mailing.db.mailing.MailingPersonalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListSet;

@Component
public class MailingScheduler
{
  private final static Logger log = LoggerFactory.getLogger(MailingScheduler.class);

  private final MailingConfigProperties propMailing;
  private final MailingPersonalService mailingPersonalService;
  private final JavaMailSender mailSender;
  private final TemplateEngine templateEngine; // Thymeleaf

  private static final ConcurrentSkipListSet<MailingPersonal> POOL = new ConcurrentSkipListSet<>();
  private final int maxAttemptsToSendIfError;

  @Value("${spring.mail.username}")
  private String sendFrom;

  @Autowired
  public MailingScheduler(MailingConfigProperties propMailing,
                          MailingPersonalService mailingPersonalService,
                          JavaMailSender mailSender,
                          TemplateEngine templateEngine)
  {
    this.propMailing = propMailing;
    this.mailingPersonalService = mailingPersonalService;
    this.mailSender = mailSender;
    this.templateEngine = templateEngine;

    this.maxAttemptsToSendIfError = propMailing.getScheduledTaskMailing().getMaxAttemptsToSendIfError();
  }


  @Scheduled(
          initialDelayString = "#{mailingConfigProperties.scheduledTaskMailing.initialDelay}",
          fixedRateString = "#{mailingConfigProperties.scheduledTaskMailing.collectFromDbInterval}")
  public void collectMailingTasksFromDatabase()
  {
    POOL.addAll(mailingPersonalService.getNewJob());
    log.debug("mailing [{}] collected from DB", POOL.size());
  }


  @Scheduled(
          initialDelayString = "#{mailingConfigProperties.scheduledTaskMailing.initialDelay}",
          fixedDelayString = "#{mailingConfigProperties.scheduledTaskMailing.sendInterval}")
  public void sendingMail()
  {
    log.debug("mailing [{}] before sending", POOL.size());
    final var toDataBase = new ArrayList<MailingPersonal>();
    POOL.forEach(next -> {
      var type = next.getType();
      var email = next.getEmail();
      var module = next.getModule();
      var data = next.getData();
      try
      {
        var subjectMessage = getSubjectMessage(type, module, data);
        MimeMessagePreparator mimeMessagePreparator = mimeMessage -> {
          var helper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_NO);
          helper.setFrom(sendFrom);
          helper.setTo(email);
          helper.setSubject(subjectMessage.get("subject"));
          helper.setText(subjectMessage.get("message"), true);
          //mimeMessage.addHeader("Content-Transfer-Encoding", "base64"); // to auto encode message to base64
          mimeMessage.addHeader("Content-Transfer-Encoding", "quoted-printable");
        };
        mailSender.send(mimeMessagePreparator);
        next.setSent(true);
        clearMailing(next);
        toDataBase.add(next);
        log.debug("mailing <{}> successfully sent, {} / {}", email, module, type);
      }
      catch (Throwable e)
      { // an error may occur, but the email will still be sent,
        // thus it is normal to send several identical emails

        next.setCauseOfLastFailure(e.toString());

        if (next.getAttempt().incrementAndGet() > maxAttemptsToSendIfError)
        {
          next.setMaxAttemptsReached(true);
          clearMailing(next); // but all of a sudden the letter was sent
          toDataBase.add(next);
          log.debug("mailing <{}> last fail sending, {} / {}: {}", email, module, type, next.getCauseOfLastFailure());
        }
        else log.debug("mailing <{}> go to attempt[{}] after fail, {} / {}: {}", email, next.getAttempt().get(), module, type, next.getCauseOfLastFailure());
      }
    });

    if (toDataBase.size() > 0)
    {
      mailingPersonalService.saveAll(toDataBase);
      POOL.removeAll(toDataBase);
      log.debug("mailing [{}] stored to DB", toDataBase.size());
      toDataBase.clear();
    }
  }

  private void clearMailing(MailingPersonal mailing)
  {
    mailing.clear();
  }

  private Map<String, String> getSubjectMessage(String type, String module, String data)
  {
    var result = new HashMap<String, String>();
    var subjKey = "subject";
    var messageKey = "message";
    var dataKey = "data";
    var context = new Context();
    var frontendHost = propMailing.getFrontend().getHost();

    if ("WELCOME_ONLY".equals(type))
    {
      result.put(subjKey, "Welcome");
      context.setVariable("account_link", frontendHost + propMailing.getFrontend().getResourceAccount());
    }
    else if ("WELCOME_PASSWORD".equals(type))
    {
      result.put(subjKey, "Welcome");
      context.setVariable(dataKey, data);
      context.setVariable("account_link", frontendHost + propMailing.getFrontend().getResourceAccount());
    }
    else if ("EMAIL_CONFIRM".equals(type))
    {
      result.put(subjKey, "Email confirm");
      var resourceConfirmEmail = propMailing.getFrontend().getResourceEmailConfirm();
      context.setVariable(dataKey, frontendHost + resourceConfirmEmail + "/" + data);
    }
    else if ("PASSWORD_WAS_CHANGED".equals(type))
    {
      result.put(subjKey, "Password has been changed");
    }
    else if ("PASSWORD_RESET_CONFIRM".equals(type))
    {
      result.put(subjKey, "Password reset");
      var resourceConfirmResetPass = propMailing.getFrontend().getResourcePasswordReset();
      context.setVariable(dataKey, frontendHost + resourceConfirmResetPass + "/" + data);
    }

    module = module
            .replaceAll("dev", "")
            .replaceAll("prod", "")
            .replaceAll("test", "");

    result.put(messageKey, templateEngine.process(module + "_mailing_" + type, context));
    return result;
  }
}
